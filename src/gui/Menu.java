package gui;

import javax.sound.sampled.BooleanControl;
import javax.swing.*;
import java.awt.*;

/*
*
*   Menu Window
*
* */

public class Menu extends JFrame {

    JLabel lbl_title = new JLabel("KKS Schach");

    /*
    *
    *   Player One Panel
    *   with textfield
    *
    * */

    JPanel pnl_player_one = new JPanel();
    JLabel lbl_player_one = new JLabel("Spieler 1", SwingConstants.CENTER);
    JTextField txtf_player_one = new JTextField(30);

    /*
     *
     *   Player Two Panel
     *   with textfield
     *
     * */

    JPanel pnl_player_two = new JPanel();
    JLabel lbl_player_two = new JLabel("Spieler 2", SwingConstants.CENTER);
    JTextField txtf_player_two = new JTextField(30);

    JPanel pnl_line_axis = new JPanel();

    Button btn_start_game = new Button("Starte Spiel");

    public Menu() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(500,500);
        setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));

        this.pnl_player_one.setLayout(new BoxLayout(this.pnl_player_one, BoxLayout.Y_AXIS));

        this.txtf_player_one.setColumns(45);

        this.pnl_player_one.add(this.lbl_player_one);
        this.pnl_player_one.add(this.txtf_player_one);

        this.pnl_player_two.setLayout(new BoxLayout(this.pnl_player_two, BoxLayout.Y_AXIS));

        this.txtf_player_two.setColumns(45);

        this.pnl_player_two.add(this.lbl_player_two);
        this.pnl_player_two.add(this.txtf_player_two);

        pnl_line_axis.setLayout(new BoxLayout(this.pnl_line_axis, BoxLayout.LINE_AXIS));
        pnl_line_axis.add(this.pnl_player_one);
        pnl_line_axis.add(this.pnl_player_two);

        add(lbl_title);
        add(pnl_line_axis);
        add(btn_start_game);
        setVisible(true);
    }
}
